
Version 2328 Saved June 21, 2019

Authors: rysiek, j, neal
Implementing kuvert in Sequoia
 
https://github.com/occrp/kuvert
 
https://gitlab.com/sequoia-pgp/umschlagend#how-it-works
https://gitlab.com/sequoia-pgp/umschlagend/blob/issue26_design/doc/design.md
 
kuvert implements an smtp server
 
 - submission agent listens on some smtp port and puts the files in a directory (kuvert_submit.c)
 
 - forked by 'kuvert' at start up
 
 
"Alternatively, you can send your email to kuvert via SMTP. Kuvert comes with a built-in
       receive-only mail server, which feeds to the queue directory."
       https://manpages.ubuntu.com/manpages/xenial/man1/kuvert.1.html
       
       https://manpages.ubuntu.com/manpages/xenial/man1/kuvert_submit.1.html
       
       https://github.com/occrp/kuvert/blob/master/Dockerfile#L56
       
OCCRP uses three kuvert instances
 
 - Why three instances?
 - Different configurations
   - Internal wiki should encrypt or drop ("mustencrypt")
   - Other should encrypt opportunistically ("fallback")
   - Another always unencyrpted, but signed ("signonly")
 
There are per-recipient rules, but no per-sender rules.  That would be nice,
then there would only be one instance.
 
  - Demux based on the username/password
 
  - Some configs have multiple users
  
  - Storing state in password might be a bad idea as some software might have
    a limit on password length
    
  - Alternative to SMTPS, then using ssh to delivery would be okay.
 
In OCCRP's case, they use the same keyring for all instances, but different
keyrings could be useful.
 
per-sender rules:
    
    Requires different username/password for each sender to identify sender
    
Key management is very bad in Kuvert
[because kuvert loads all the keys in memory?]
 
  - When keys change, kuvert has to be reloaded:
      https://github.com/occrp/kuvert/blob/master/run.sh#L50
      
      Currently, OCCRP has their own script that uses inotify to watch the keyring
      
Stats: Currently 10k users (but not all have keys)
 
Ideally: kuvert runs on a single machine and different services talk to it over the network
 
 - Important to simplify the keyring management
 
 Logging
 
   Logging is important
   kuvert logging is working, have to use debug but too verbose
   
   log shound contain:
       email receiver from user X
       email sent to Y
       what key was used for signing/encrypting
       encrypted? signed?
       don't want the cleartext logged (unless debug mode?)
   
Would be nice if the software ran in the foregroup
 
  - Necessary for docker/kubernettes deployment
  
  
kuvert-signonly_1     | Net::SMTP::_SSL=GLOB(0x55c4ce088648)<<< 250 2.0.0 OK  1561107695 o20sm5886818wrh.8 - gsmtp
kuvert-signonly_1     | Net::SMTP::_SSL=GLOB(0x55c4ce088648)>>> QUIT
kuvert-signonly_1     | Net::SMTP::_SSL=GLOB(0x55c4ce088648)<<< 221 2.0.0 closing connection o20sm5886818wrh.8 - gsmtp
kuvert-signonly_1     | DEBUG: .../IO/Socket/SSL.pm:2845: free ctx 94303758772512 open=
kuvert-signonly_1     | DEBUG: .../IO/Socket/SSL.pm:2849: free ctx 94303758772512 callback
kuvert-signonly_1     | DEBUG: .../IO/Socket/SSL.pm:2856: OK free ctx 94303758772512
 
All instances uses the same "From":
    
Want per instance rules so that they use different from fields
  Wiki -> kuvert-mustencrypt -> notifications@occrp.org
  Cloud -> kuvert-fallback -> notifications@occrp.org
  SSO -> kuvert-signonly -> sso@occrp.org
  
Want per instance rules so that they use different from fields
 
  - Then you can use different signing keys
  
  
  OCCRP key management:
      
    - List of all keys suspected to be correct
    - List of verified keys
    
    - keys in the git repo are stored in files named after the verified email address
    - if a key has multiple verified emails, then there are N files in the git repo,
      one for each verified email address
    
    
Opportunistic key discovery:
    
    Yes, but it should be configurable at a fine-grained level (e.g., wkd ok, public keyservers, ...)
    And it should be configurable for each sender
    
    default: everything is enabled
    
Automatic key updates:
    
    e.g., looking for revocation certs on key servers, etc.
    Should be handled in the same way as opportunistic key discovery (i.e., configurable)
    
    default: everything is enabled
    
Expiration/ reovcation handling
 
 - Ideas: encrypt anyway?
 - always log that fact
 - Send email to user saying: "hey your key expired" (without the original message) (default, optionally possible to disable)
- Perhaps also send an email to the admin that a key has expired. (optionally configurable)
 
 Make behavior configurable per config
 
 Default should be the same for different "modes"
  
  
 Client -> piece of software connecting to Umschlagend via SMTP or whatnot to send an e-mail
 Config -> a complete set of configuration directives used for one or more Clients
 Upstream SMTP Config -> an upstream SMTP server configuration (server, port, credentials, etc) used for actually sending the (signed? encrypted?) e-mail. One per Config.
 Keyring -> a set of keys.  This is one keyring per config and it is named implicitly
 Mode -> signonly; fallback to cleartext; mustencrypt (these are the kuvert modes; in Umschalgend, they could be different. In particular, configuring encryption and signing separately makes sense)
   encryption -> none, opportunistic, mandatory (drop otherwise)
   signing -> list of 0 or more keys; default: sign with all private keys that have the email address matching the Upstream SMTP Config e-mail address
 
 openpgpkey.... is the WKD advanced method
 the keys are in occrp.org (using direct method)
 path is different
redirect bug in our wkd client
or just remove openpgp... domain (it's a wildcard...)
micro is not working
 
Good documentation
 
Testing for the admin
 
  - It would be nice to have a test script that prints out what the server does and doesn't do
    dump the (explicit) configuration, with default options shown explicitly
    
    
Secret key management:
    
    Currently, they use unencrypted secret keys
    Would be interesting to (optionally) use password protection and require the admin to decrypt when starting the service
 

