umschlagend.toml - the umschlagend configuration file
======================================================

DESCRIPTION
-----------

A configuration file in TOML ⟨https://github.com/toml-lang/toml⟩ format
to specify a variety of configuration options for umschlagend. Resides
at $HOME/.umschlagend.toml.

FORMAT
------

See the TOML GitHub Repository ⟨https://github.com/toml-lang/toml⟩ for
details about the syntax of the configuration file.

CONFIG
------

The configuration is specified in sections which each have their own
top-level tables ⟨https://github.com/toml-lang/toml\#table⟩ , with
key/value pairs specified in each section.

Example:

```
log_level = "trace"

[send_mta]
domain = "localhost"
port = 25

[send_mta.credentials]
username = "foo"
password = "bar"

[[clients]
from = "application\@localhost.localdomain"
crypto_action = "Encrypt"

[[clients]
from = "sign\@localhost"
crypto_action = "Sign"

[[clients]
from = "signencrypt\@localhost"
crypto_action = "SignEncrypt"

[[clients]
from = "signencrypt\@localhost"
crypto_action = "Encrypt"
send_all_or_none = true
```

SEE ALSO
--------

**umschlagend** (1), *https://https://juga0.gitlab.io/umschlagend/*

BUGS
----

Please report bugs at https://gitlab.com/juga0/umschlagend/issues

AUTHOR
------

juga [juga at riseup dot net]

COPYRIGHT
---------

GPLv3
