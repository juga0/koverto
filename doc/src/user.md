# User documentation

- [Installation](INSTALL.md)
- [`umschlagend` Manual page](umschlagend.1.md)
- [Configuration manual page](umschlagend.toml.5.md)
