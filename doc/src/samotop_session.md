# Samotop session

Run:

```text
git clone https://gitlab.com/BrightOpen/BackYard/Samotop/
cargo run
```

In other terminal:

```text
telnet localhost 12345
Trying ::1...
Connected to localhost.
Escape character is '^]'.
220 machine Service ready
helo localhost
250 machine greets localhost
mail from:<me@there.net>
250 Ok
rcpt to:<@relay.net:him@unreachable.local>
250 Ok
data
354 Start mail input, end with <CRLF>.<CRLF>
test
.
250 Queued as 9c8531bc-f1d8-46cc-9845-c88ff3d0df95
quit
221 machine Service closing transmission channel
Connection closed by foreign host.
```

First terminal will show:

```text
Accepting recipient AcceptRecipientRequest { name: "machine", local: Some(V6([::1]:12345)), peer: Some(V6([::1]:57876)), helo: Some(Helo(Domain("localhost"))), mail: Some(Mail(Direct(Mailbox("me", Domain("there.net"))))), id: "9c8531bc-f1d8-46cc-9845-c88ff3d0df95", rcpt: Relay([Domain("relay.net")], Mailbox("him", Domain("unreachable.local"))) }
Mail from <me@there.net> (helo: localhost, mailid: 9c8531bc-f1d8-46cc-9845-c88ff3d0df95) (peer: [::1]:57876) for <him@unreachable.local>,  on machine ([::1]:12345 <- [::1]:57876)
Mail data for 9c8531bc-f1d8-46cc-9845-c88ff3d0df95: b"test"
Mail data finished for 9c8531bc-f1d8-46cc-9845-c88ff3d0df95
```
