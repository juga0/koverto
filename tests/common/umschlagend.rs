use super::{log_level, stdout};
use rand::Rng;
use std::io::prelude::*;
use std::{ffi, fs, iter, panic, path, process};
use umschlagend_lib::tests_common;

/// An umschlagend environment.
///
/// Creates a TempDir as home and seeds it.
/// Umschlagend processes can be started with `run(&[subcommand])`.
/// All processes will be killed when the struct goes out of scope.
pub struct Umschlagend {
    procs: Vec<process::Child>,
    send_to: Option<u16>,
    pub home: tempfile::TempDir,
    pub listen_on: u16,
}

impl Umschlagend {
    pub fn new(send_to: Option<u16>) -> Self {
        let umschlagend = Self {
            procs: vec![],
            send_to,
            home: tests_common::home_with_fixtures(),
            listen_on: rand::thread_rng().gen_range(25000, 25999),
        };
        umschlagend.write_config();
        umschlagend
    }

    /// Start the main umschlagend process
    pub fn start(self: &mut Self) {
        self.run(iter::empty::<&str>());
    }

    /// Run an umschlagend subcommand in the background
    pub fn run<I, S>(self: &mut Self, args: I)
    where
        I: IntoIterator<Item = S>,
        S: AsRef<ffi::OsStr>,
    {
        self.procs.push(
            process::Command::new("target/debug/umschlagend")
                .args(&["--log-level", log_level()])
                .args(args)
                .env("HOME", self.home.path())
                .stdout(stdout())
                .spawn()
                .unwrap(),
        );
    }

    /// Execute an umschlagend subcommand and wait for it to finish
    pub fn exec<I, S>(self: &mut Self, args: I)
    where
        I: IntoIterator<Item = S>,
        S: AsRef<ffi::OsStr>,
    {
        let cmd = process::Command::new("target/debug/umschlagend")
            .args(&["--log-level", log_level()])
            .args(args)
            .env("HOME", self.home.path())
            .stdout(stdout())
            .output()
            .unwrap();
        assert!(cmd.status.success(), "{:?}", cmd);
    }

    /// Configure umschlagend in home to use port for sending mails.
    ///
    /// Configuration will be appended to home/.umschlaged/
    fn write_config(self: &Self) {
        let path = self.home.path().join(".umschlagend.toml");
        let mut file = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .open(&path)
            .unwrap();
        write!(&mut file, "\n[receive_mta]\nport = {}\n", self.listen_on)
            .expect("failed to write config");
        if let Some(port) = self.send_to {
            write!(&mut file, "\n[send_mta]\nport = {}\n", port)
                .expect("failed to write config");
        }
    }

    /// Queue dir of this umschlagend instance.
    pub fn queue(self: &Self) -> path::PathBuf {
        self.home.path().join(".mail")
    }
}

impl Drop for Umschlagend {
    fn drop(&mut self) {
        for proc in self.procs.iter_mut() {
            proc.kill().unwrap();
        }
    }
}
