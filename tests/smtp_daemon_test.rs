//! Tests for umschlagend as a smtp receiving server
//!
//! These tests start umschlagend services via the command line API,
//! start an additional MTA to receive mails from umschlagend,
//! feed mails to umschlagend with `swaks`,
//! and then check for mails received as a result.

mod common;

use common::{fetch_mail, wait_for_processing, SmtpSink, Umschlagend};
use std::io::ErrorKind;
use std::process;

#[test]
fn queue_mail() {
    let mut umschlagend = Umschlagend::new(None);
    umschlagend.run(&["mta"]);
    send_mail(umschlagend.listen_on);
    assert!(fetch_mail(&umschlagend.queue()).unwrap().is_ok());
}

#[test]
fn process_mail() {
    let mut umschlagend = Umschlagend::new(None);
    umschlagend.run(&["mta"]);
    umschlagend.start();
    wait_for_processing(&umschlagend.queue(), || {
        send_mail(umschlagend.listen_on);
    });
    assert!(fetch_mail(&umschlagend.queue()).is_none());
}

#[test]
fn encrypt_mail() {
    let mut destination = SmtpSink::new();
    destination.start();
    let mut proxy = Umschlagend::new(Some(destination.listen_on));
    let keyring = proxy.home.path().join(".umschlagend/keyring");
    proxy.exec(&["import", keyring.to_str().unwrap()]);
    proxy.run(&["mta"]);
    proxy.start();
    wait_for_processing(&proxy.queue(), || {
        send_mail(proxy.listen_on);
    });
    assert!(fetch_mail(&destination.queue()).is_some());
    assert!(fetch_mail(&proxy.queue()).is_none());
}

#[test]
fn drop_mail_if_key_is_missing() {
    let mut destination = SmtpSink::new();
    destination.start();
    let mut proxy = Umschlagend::new(Some(destination.listen_on));
    proxy.run(&["mta"]);
    proxy.start();
    wait_for_processing(&proxy.queue(), || {
        send_mail(proxy.listen_on);
    });
    assert!(fetch_mail(&proxy.queue()).is_none());
    assert!(fetch_mail(&destination.queue()).is_none());
}

const SWAKS_NOT_FOUND: &str = "Could not find swaks! Is it installed?";

/// Use swaks to send a mail.
///
/// The mail will fit the default config of umschlagend.
/// That is it will use the same server:port
/// and a from address umschlagend is configured for.
fn send_mail(port: u16) {
    let mut swaks = process::Command::new("swaks");
    swaks
        .args(&["--from", "application@localhost.localdomain"])
        .args(&["--to", "user@localhost"])
        .args(&["--server", "127.0.0.1"])
        .args(&["--port", &port.to_string()]);
    match swaks.output() {
        Ok(ref output) if output.status.success() => (),
        Err(ref err) if err.kind() == ErrorKind::NotFound => {
            panic!(SWAKS_NOT_FOUND)
        }
        _ => panic!("Failed to send mail with: `{:?}`", swaks),
    }
}
