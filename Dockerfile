FROM rust:slim-buster
MAINTAINER juga <juga at riseup dot net>
WORKDIR /usr/umschlagend
# install all dependencies first so this layer can be reused.
# To update the installed packages run docker build --no-cache=true
RUN DEBIAN_FRONTEND=noninteractive apt update && \
    apt install -yq --no-install-recommends \
        capnproto clang make pkg-config nettle-dev \
        libssl-dev libsqlite3-dev libssl1.1 openssl && \
    apt -yq autoremove && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

# Cache the dependencies
COPY Cargo.toml Cargo.lock ./
RUN mkdir src; touch src/lib.rs; echo "fn main () {}" > src/main.rs; \
  cargo build --release; \
  rm -rf src

# This will not copy anything in .dockerignore
COPY . .
RUN touch src/lib.rs src/main.rs ; \
  make build && make install && make clean

# For some reason /bin/systemctl is not found when building the docker image
# even if systemd is installed.
# ENV WITH_SYSTEMD 1
ENV RUST_BACKTRACE 1

RUN adduser --disabled-password --gecos "" umschlagend;

USER umschlagend:umschlagend
WORKDIR /home/umschlagend

RUN  mkdir .mail .umschlagend

VOLUME /home/umschlagend/.mail /home/umschlagend/.umschlagend
EXPOSE 25465

CMD ["/usr/local/bin/umschlagend"]
