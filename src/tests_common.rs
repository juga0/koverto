//! Common functions for unit and integration tests.
//!
//! They are public so that they can be used in the integration tests outside
//! the crate.
use super::*;
use crate::constants::DATA_DIR;
use std::fs;

const FIXTURE_DIR: &str = "tests/data";
pub const USER_EMAIL: &str = "user@localhost";

/// Create a TempDir that can be used as home for umschlagend
///
/// Seeds the dir with the content of tests/data/.umschlagend.
/// Namely:
///  * the test keyring,
///  * secret key, and
///  * tls cert fixtures
///
pub fn home_with_fixtures() -> tempfile::TempDir {
    let home = tempfile::tempdir().unwrap();
    let data = home.path().join(DATA_DIR);
    fs::create_dir_all(&data).unwrap();
    for entry in fs::read_dir(&fixture(DATA_DIR)).unwrap() {
        let path = entry.unwrap().path();
        let dest = data.join(&path.file_name().unwrap());
        fs::copy(&path, &dest).unwrap();
    }
    home
}

pub fn fixture(name: &str) -> path::PathBuf {
    fixture_path().join(name)
}

pub fn fixture_path() -> path::PathBuf {
    path::PathBuf::from(FIXTURE_DIR)
}

pub mod fixture {
    use super::fixture_path;
    use crate::constants::DATA_DIR;
    use crate::{config, email, store};
    use failure::Fallible;
    use std::{fs, path};

    /// Create a TempDir that can be used as home for umschlagend
    ///
    /// Seeds the dir with the content of tests/data/.umschlagend.
    /// Namely:
    ///  * the test keyring,
    ///  * secret key, and
    ///  * tls cert fixtures
    ///
    /// Also loads the contents of the keyring into the store.
    ///
    pub fn home() -> Fallible<tempfile::TempDir> {
        let home = tempfile::tempdir()?;
        let data = home.path().join(DATA_DIR);
        fs::create_dir_all(&data)?;
        for entry in fs::read_dir(&path(DATA_DIR))? {
            let path = entry.unwrap().path();
            let dest = data.join(&path.file_name().unwrap());
            fs::copy(&path, &dest)?;
        }
        let cfg = config::Config::from(&home);
        let keys_store = store::Store::new(&cfg.store_config)?;
        keys_store.import_keyring(cfg.keyring_path())?;
        assert!(keys_store.obtain_keys("user@localhost").is_ok());
        Ok(home)
    }

    pub fn incoming_email() -> Fallible<email::Incoming> {
        let email_string =
            fs::read_to_string("tests/data/email_plain_single.eml")?;
        email::Incoming::new(email_string)
    }

    pub fn path(name: &str) -> path::PathBuf {
        fixture_path().join(name)
    }
}

pub mod assert {
    use failure::Fallible;
    pub fn err_message<T>(res: Fallible<T>, message: &str) {
        match res {
            Ok(_) => panic!("\nExpected error did not happen."),
            Err(err) => assert_eq!(format!("{}", err), message),
        }
    }
}
