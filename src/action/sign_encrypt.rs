use crate::{config, email, openpgp, store};
use failure::{Fallible, ResultExt};
use lettre_email;

pub struct SignEncrypt {
    secret_key: sequoia_openpgp::Cert,
    keys_store: store::Store,
}

impl SignEncrypt {
    pub fn new(
        config: &config::Config,
        client: &config::Client,
    ) -> Fallible<Self> {
        let secret_key = store::obtain_secret_key(&client.from, &config)?;
        let keys_store = store::Store::new(&config.store_config)?;
        Ok(Self {
            secret_key,
            keys_store,
        })
    }
}

impl super::CryptoAction for SignEncrypt {
    fn builder(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder> {
        let (ciphertext, encryption_key, signing_key) = openpgp::sign_encrypt(
            &source,
            &self.keys_store,
            recipient,
            &self.secret_key,
        )
        .with_context(|e| format!("Failed to sign and encrypt. {}", e))?;
        let email_builder =
            self.plain_builder(&source, recipient).with_context(|e| {
                format!(
                    "with signing key: {} and encryption key: {} {}",
                    signing_key.unwrap_or_else(|| "missing".to_string()),
                    encryption_key.unwrap_or_else(|| "missing".to_string()),
                    e
                )
            })?;
        Ok(email_builder.openpgp_encrypted(ciphertext))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::action::CryptoAction;
    use crate::config;
    use crate::tests_common::{assert, fixture};
    use tempfile;

    #[test]
    fn err_flow() -> Fallible<()> {
        let empty_home = tempfile::TempDir::new()?;
        let cfg = config::Config::from(&empty_home);
        let client = &cfg.clients[0];
        let sign_encrypt = SignEncrypt::new(&cfg, &client)?;
        let incoming = fixture::incoming_email()?;
        let recipient = "user@localhost";
        assert::err_message(
            sign_encrypt.build(&incoming, recipient),
            "Failed to sign and encrypt. No usable encryption key: Key not found");
        Ok(())
    }

    #[test]
    fn happy_path() -> Fallible<()> {
        let home = fixture::home()?;
        let cfg = config::Config::from(&home);
        let client = &cfg.clients[0];
        let sign_encrypt = SignEncrypt::new(&cfg, &client)?;
        let incoming = fixture::incoming_email()?;
        let recipient = "user@localhost";
        sign_encrypt.build(&incoming, recipient)?;
        Ok(())
    }
}
