use crate::{constants, email};
use failure::Fallible;

mod encrypt;
mod sign;
mod sign_encrypt;

pub use encrypt::Encrypt;
pub use sign::Sign;
pub use sign_encrypt::SignEncrypt;

pub trait CryptoAction {
    fn builder(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder>;

    // #3: Include headers from the original email
    fn plain_builder(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder> {
        let date = time::strptime(&source.date(), constants::DATE_FMT)?;
        Ok(lettre_email::EmailBuilder::new()
            .from(source.from())
            .to(recipient)
            .subject(source.subject())
            .date(&date))
    }

    fn build(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre::SendableEmail> {
        Ok(self.builder(&source, &recipient)?.build()?.into())
    }
}
