use crate::{config, email, openpgp, store};
use failure::{Fallible, ResultExt};
use lettre_email;
use sequoia_openpgp;

pub struct Sign {
    secret_key: sequoia_openpgp::Cert,
}

impl Sign {
    pub fn new(
        config: &config::Config,
        client: &config::Client,
    ) -> Fallible<Self> {
        let secret_key = store::obtain_secret_key(&client.from, &config)?;
        Ok(Self { secret_key })
    }
}

impl super::CryptoAction for Sign {
    fn builder(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder> {
        let (signature, _, sign_key) =
            openpgp::sign_only(source.body(), &self.secret_key)
                .with_context(|e| format!("Failed to sign. {}", e))?;
        let email_builder =
            self.plain_builder(&source, recipient).with_context(|e| {
                format!(
                    "with signing key {} {}",
                    sign_key.unwrap_or_else(|| "missing".to_string()),
                    e
                )
            })?;
        // As of Sep 2019, Sequoia always use sha512
        let sendable = email_builder.openpgp_signed(
            source.body(),
            signature,
            "pgp-sha512",
        );
        Ok(sendable)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::action::CryptoAction;
    use crate::config;
    use crate::tests_common::{assert, fixture};
    use tempfile;

    // TODO: create a failing test case.
    fn _err_flow() -> Fallible<()> {
        let empty_home = tempfile::TempDir::new()?;
        let cfg = config::Config::from(&empty_home);
        let client = &cfg.clients[0];
        let sign = Sign::new(&cfg, &client)?;
        let incoming = fixture::incoming_email()?;
        let recipient = "user@localhost";
        assert::err_message(
            sign.build(&incoming, recipient),
            "Failed to sign. No usable key: Key not found",
        );
        Ok(())
    }

    #[test]
    fn happy_path() -> Fallible<()> {
        let home = fixture::home()?;
        let cfg = config::Config::from(&home);
        let client = &cfg.clients[0];
        let sign = Sign::new(&cfg, &client)?;
        let incoming = fixture::incoming_email()?;
        let recipient = "user@localhost";
        sign.build(&incoming, recipient)?;
        Ok(())
    }
}
