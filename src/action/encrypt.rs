use crate::{config, email, openpgp, store};
use failure::{Fallible, ResultExt};
use lettre_email;

pub struct Encrypt {
    keys_store: store::Store,
}

impl Encrypt {
    pub fn new(cfg: &config::Config) -> Fallible<Self> {
        let keys_store = store::Store::new(&cfg.store_config)
            .with_context(|e| format!("Failed to load key store {}", e))?;
        Ok(Self { keys_store })
    }
}

impl super::CryptoAction for Encrypt {
    fn builder(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder> {
        trace!("Encrypting...");
        let (ciphertext, key, _) =
            openpgp::encrypt_only(&source, &self.keys_store, &recipient)
                .with_context(|e| format!("Failed to encrypt. {}", e))?;
        let email_builder =
            self.plain_builder(&source, recipient).with_context(|e| {
                format!(
                    "Failed to build email (encryption key {}) {}",
                    key.unwrap_or_else(|| "missing".to_string()),
                    e
                )
            })?;
        Ok(email_builder.openpgp_encrypted(ciphertext))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::action::CryptoAction;
    use crate::config;
    use crate::tests_common::{assert, fixture};
    use tempfile;

    #[test]
    fn err_flow() -> Fallible<()> {
        let empty_home = tempfile::TempDir::new()?;
        let cfg = config::Config::from(&empty_home);
        let encrypt = Encrypt::new(&cfg)?;
        let incoming = fixture::incoming_email()?;
        let recipient = "user@localhost";
        assert::err_message(
            encrypt.build(&incoming, recipient),
            "Failed to encrypt. No usable key: Key not found",
        );
        Ok(())
    }

    #[test]
    fn happy_path() -> Fallible<()> {
        let home = fixture::home()?;
        let cfg = config::Config::from(&home);
        let encrypt = Encrypt::new(&cfg)?;
        let incoming = fixture::incoming_email()?;
        let recipient = "user@localhost";
        encrypt.build(&incoming, recipient)?;
        Ok(())
    }
}
