//! Library main functions.

#[macro_use]
extern crate clap;
#[macro_use]
extern crate log;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate prettytable;

use failure::Fallible;
use std::path;

mod action;
pub mod cli;
pub mod config;
pub mod constants;
pub mod email;
pub mod error;
pub mod openpgp;
pub mod processor;
pub mod receive_mta;
pub mod receive_queue;
pub mod send_mta;
pub mod store;
pub mod subcommands;
// To be used in unit and integration tests
pub mod tests_common;

/// Main function that returns errors.
pub fn run() -> Fallible<()> {
    // We still need to load the config and configure the logger.
    // It does not seem possible to change the log level
    // after initializing the logger with `log` and `pretty-env-logger`.
    // So we use `println` here because we cannot predict the log level.
    // Issue #52 addresses logging to a file and might also tackle this.
    println!("📧🔑 Starting umschlagend...");

    // Create cli arguments parser.
    let opt = cli::parse();

    // Parse config argument.
    let config_path = match opt.config.clone() {
        Some(path_str) => path_str,
        None => config::default_config_path(),
    };

    // Load configuration
    let cfg = match config::Config::load(&config_path) {
        Err(error) => {
            if opt.config.is_some() {
                let error_message = format_err!(
                    "Error loading configuration {}: {}.",
                    config_path.to_str().unwrap(),
                    error
                );
                return Err(error_message);
            } else {
                println!("Using configuration defaults");
                config::Config::new()
            }
        }
        Ok(cfg) => {
            println!(
                "Using configuration file {}.",
                config_path.to_str().unwrap()
            );
            cfg
        }
    };
    println!("Configuration: {}", toml::to_string(&cfg).unwrap());

    // Once we have a way to adjust the log level
    // we should start with a default log level above (see comment)
    // and change it here rather than setting it.
    let log_level = config::LevelFilterWrapper::from(
        opt.log_level
            .unwrap_or_else(|| cfg.log_level.to_string())
            .as_str(),
    );

    pretty_env_logger::formatted_builder()
        .filter(Some(&cfg.log_module), log_level.0)
        // Because samotop does not return errors, log them.
        .filter(Some("samotop"), log::LevelFilter::Warn)
        .init();

    match opt.subcommand {
        Some(subcmd) => subcommands::handle(subcmd, &cfg),
        None => watch_queue(&cfg),
    }
}

fn watch_queue(cfg: &config::Config) -> Fallible<()> {
    let client = cfg
        .clients
        .first()
        .expect("There must be at least one client - or the default one.");
    let processor = processor::Processor::new(&cfg, &client)?;
    let path = &cfg.home_path.join(&client.directory);
    match receive_queue::ReceiveQueue::new(path, processor) {
        Ok(queue) => queue.watch(),
        Err(err) => {
            error!("Failed to watch queue dir: {}", err);
            Err(err)
        }
    }
}
