//! Functions to parse/build Emails and MIME.

mod incoming;

pub use incoming::Incoming;
