use serde::{Deserialize, Serialize};

/// Actions to do with an email: encrypt and/or sign
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum CryptoAction {
    Encrypt,
    Sign,
    SignEncrypt,
}

/// Default email action is to encrypt
impl Default for CryptoAction {
    fn default() -> Self {
        CryptoAction::Encrypt
    }
}
