use serde::{Deserialize, Serialize};
use std::ops::Deref;
use std::path;

use crate::config::paths::user_home;

/// `PathBuf` wrapper used in
/// [Config.home_path](struct.Config.html#structfield.home_path)
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct HomePath(pub path::PathBuf);

impl Default for HomePath {
    fn default() -> Self {
        HomePath(user_home())
    }
}

impl Deref for HomePath {
    type Target = path::PathBuf;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl AsRef<path::Path> for HomePath {
    fn as_ref(&self) -> &path::Path {
        &self.0.as_path()
    }
}

#[cfg(test)]
mod tests {
    use super::HomePath;
    use crate::config::paths::user_home;

    #[test]
    fn home_path_deref() {
        let dp = HomePath::default();
        let p = user_home();
        // /home/foo/
        assert_eq!(dp.to_string_lossy(), p.to_string_lossy());
    }
}
