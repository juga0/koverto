use serde::{Deserialize, Serialize};
use std::ops::Deref;

use crate::constants::LOG_MODULE;

/// `String` wrapper used in
/// [Config.log_module](struct.Config.html#structfield.log_module)
///
/// To be able to use `#[serde(default)]`
/// so that the key is not required in the configuration file
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct LogModule(pub String);

impl Default for LogModule {
    fn default() -> Self {
        LogModule(LOG_MODULE.to_owned())
    }
}

impl Deref for LogModule {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
