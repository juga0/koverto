use crate::config::paths::{data_path_from_home, default_data_dir};
use crate::config::PortWrapper;
use crate::constants::{RECEIVE_MTA_ADDRESS, RECEIVE_MTA_TLS_CERT_FILE};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::ops::Deref;
use std::path;

/// MTA configuration for receiving Emails.
#[derive(Serialize, Deserialize, Debug, PartialEq, Default)]
pub struct ReceiveMTA {
    #[serde(default = "default_receive_mta_certificate_path")]
    /// Filse system path to a TLS certificate formatted as PFX.
    pub tls_certificate_path: path::PathBuf,
    #[serde(default)]
    /// A domain name or IP address.
    pub address: AddressWrapper,
    /// A network socket port.
    #[serde(default)]
    pub port: PortWrapper,
}

impl<P> From<P> for ReceiveMTA
where
    P: AsRef<path::Path>,
{
    fn from(user_home: P) -> Self {
        ReceiveMTA {
            tls_certificate_path: tls_certificate_path_from_home(user_home),
            address: AddressWrapper::default(),
            port: PortWrapper::default(),
        }
    }
}

impl fmt::Display for ReceiveMTA {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // SamotopBuilder.on() expects a socket address String
        write!(f, "{}:{}", self.address.to_string(), self.port.to_string())
    }
}

/// `String` wrapper used in
/// [ReceiveMTA.address](struct.ReceiveMTA.html#structfield.address)
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct AddressWrapper(String);

impl Default for AddressWrapper {
    fn default() -> Self {
        AddressWrapper(RECEIVE_MTA_ADDRESS.to_owned())
    }
}

impl Deref for AddressWrapper {
    type Target = String;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub fn tls_certificate_path_from_home<P: AsRef<path::Path>>(
    home_path: P,
) -> path::PathBuf {
    data_path_from_home(home_path).join(RECEIVE_MTA_TLS_CERT_FILE)
}

// Not being used for now
fn default_receive_mta_certificate_path() -> path::PathBuf {
    default_data_dir().join(RECEIVE_MTA_TLS_CERT_FILE)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::constants::{DATA_DIR, RECEIVE_MTA_ADDRESS, RECEIVE_MTA_PORT};
    use crate::tests_common;

    #[test]
    fn receive_mta_default() {
        let receive_mta = ReceiveMTA::default();
        let expected_receive_mta = ReceiveMTA {
            tls_certificate_path: path::PathBuf::from(""),
            address: AddressWrapper(RECEIVE_MTA_ADDRESS.to_owned()),
            port: PortWrapper(RECEIVE_MTA_PORT),
        };
        assert_eq!(receive_mta, expected_receive_mta);
    }

    #[test]
    fn receive_mta_from() {
        let home = tests_common::home_with_fixtures();
        let receive_mta = ReceiveMTA::from(&home);
        let expected_cert_path =
            home.path().join(DATA_DIR).join(RECEIVE_MTA_TLS_CERT_FILE);
        let expected_receive_mta = ReceiveMTA {
            tls_certificate_path: expected_cert_path,
            address: AddressWrapper(RECEIVE_MTA_ADDRESS.to_owned()),
            port: PortWrapper(RECEIVE_MTA_PORT),
        };
        assert_eq!(receive_mta, expected_receive_mta);
    }
}
