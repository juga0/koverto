/// Possible errors processing an Email.
#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "No action configured for sender")]
    NoAction,

    #[fail(display = "Error encrypting with key: {}", _0)]
    EncryptError(String),

    #[fail(display = "Error signing with key: {}", _0)]
    SignError(String),

    #[fail(display = "Error signing and encrypting with keys: {}", _0)]
    SignEncryptError(String),

    #[fail(display = "No Date header")]
    NoDateError,

    #[fail(display = "Invalid date")]
    InvalidDateError,

    #[fail(display = "No From header")]
    NoFromError,

    #[fail(display = "No To header")]
    NoToError,
}
