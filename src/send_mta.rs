//! MTA client to send the processed Emails.

use failure::Fallible;
use lettre;
use lettre::smtp;
use lettre::Transport;
use native_tls::TlsConnector;

use super::config;

/// Send a single Email.
pub fn send_email(
    send_mta_config: &config::SendMTA,
    sendable_email: lettre::SendableEmail,
) -> Fallible<()> {
    let mut client;
    // In case it's needed to don't use TLS in localhost, create the client as
    // client = lettre::SmtpClient::new_unencrypted_localhost()?;

    // Not using `new_simple`to allow different ports
    // client = lettre::SmtpClient::new_simple(&send_mta_config.domain)?;
    let tls_connector = TlsConnector::new().unwrap();
    let tls = lettre::ClientTlsParameters::new(
        send_mta_config.domain.clone(),
        tls_connector,
    );
    client = lettre::SmtpClient::new(
        send_mta_config.to_socket_addr().as_str(),
        lettre::smtp::ClientSecurity::Opportunistic(tls),
    )
    .unwrap();

    // Use the credentials if there're
    if let Some(c) = &send_mta_config.credentials {
        let credentials = smtp::authentication::Credentials::new(
            c.username.clone(),
            c.password.clone(),
        );
        client = client.credentials(credentials);
    }

    let mut transport = client.transport();
    match transport.send(sendable_email) {
        Ok(_) => Ok(()),
        Err(e) => Err(e.into()),
    }
}

/// Send a vector of Emails.
pub fn send_emails(
    send_mta_config: &config::SendMTA,
    sendable_emails: Vec<lettre::SendableEmail>,
) -> Vec<Fallible<()>> {
    let mut results = vec![];
    for email in sendable_emails {
        results.push(send_email(send_mta_config, email));
    }
    results
}
