//! Processor - processes the emails
//!
//! There's one processor per configured client.
//! It processes all incoming emails from this client.

use crate::error::Error;
use crate::{action, config, email, receive_queue, send_mta};
use failure::{Fallible, ResultExt};

// We actually need:
// * the send_mta config.
// * an action to perform for an incoming mail.
pub struct Processor<'a> {
    cfg: &'a config::Config,
    client: &'a config::Client,
    action: Box<dyn action::CryptoAction>,
}

impl<'a> Processor<'a> {
    pub fn new(
        cfg: &'a config::Config,
        client: &'a config::Client,
    ) -> Fallible<Self> {
        let crypto_action = &client.crypto_action;
        debug!("Selected action: {:?}", crypto_action);
        let action: Box<dyn action::CryptoAction> = match crypto_action {
            config::CryptoAction::Encrypt => {
                Box::new(action::Encrypt::new(cfg)?)
            }
            config::CryptoAction::Sign => {
                Box::new(action::Sign::new(cfg, client)?)
            }
            config::CryptoAction::SignEncrypt => {
                Box::new(action::SignEncrypt::new(cfg, client)?)
            }
        };
        Ok(Self {
            cfg,
            client,
            action,
        })
    }
    /// Process the incoming email.
    ///
    /// It logs the errors and still returns them.
    ///
    /// From [RFC2822]:
    ///
    /// ```text
    /// The only required header fields are the origination date field and
    /// the originator address field(s).  All other header fields are
    /// syntactically optional.
    /// ```
    ///
    /// [RFC2822]: https://tools.ietf.org/html/rfc2822#section-2.2
    pub fn process_email<S>(&self, email: S) -> Fallible<()>
    where
        S: AsRef<str>,
    {
        trace!("Processing email: {}", email.as_ref());
        let incoming = email::Incoming::new(email)?;
        if incoming.from() != &self.client.from {
            return Err(Error::NoAction.into());
        }
        let build_results =
            incoming.recipients().into_iter().map(|recipient| {
                trace!("Building email to recipient {}", &recipient);
                self.action.build(&incoming, &recipient).with_context(|e| {
                    format!("{} when composing to {}.", e, &recipient)
                })
            });
        if self.client.send_all_or_none {
            for result in build_results.clone() {
                result.with_context(|e| {
                    format!("No email sent cause at least one recipient failed: {}", e)
                })?;
            }
        }
        let sendables =
            build_results.filter_map(|build_result| match build_result {
                Ok(sendable_email) => {
                    trace!("Successfully built email.");
                    Some(sendable_email)
                }
                Err(e) => {
                    warn!("{}", e);
                    None
                }
            });
        for sendable in sendables {
            let description = self.sendable_description(&sendable);
            match send_mta::send_email(&self.cfg.send_mta, sendable) {
                Ok(_) => debug!("Successfully send {}.", description),
                Err(e) => error!("Error sending {}. {}", description, e),
            };
        }
        Ok(())
    }

    fn sendable_description(
        &self,
        sendable: &lettre::SendableEmail,
    ) -> String {
        let to = &sendable.envelope().to()[0];
        let from = &sendable
            .envelope()
            .from()
            .map_or("missing sender".to_string(), |to| to.to_string());
        format!("email from {} to {}", from, to)
    }
}

impl receive_queue::Handler for Processor<'_> {
    fn handle(&self, s: String) -> Fallible<()> {
        self.process_email(s)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests_common;
    use std::fs;

    #[test]
    fn process_email_succeed() {
        let home = tests_common::home_with_fixtures();
        let mut cfg = config::Config::from(&home);
        let email_string =
            fs::read_to_string("tests/data/email_plain.eml").unwrap();
        // Encrypt.
        let client = &cfg.clients.first().expect("theres should be a client");
        let processor = Processor::new(&cfg, client).unwrap();
        let email = processor.process_email(&email_string);
        // NOTE: The mime structure can not be parsed back by MimeMessage
        assert!(email.is_ok());
        // println!("{}", email.unwrap().message_to_string().unwrap());

        let mut client = config::Client::default();
        client.crypto_action = config::CryptoAction::Sign;
        // Sign.
        let processor = Processor::new(&cfg, &client).unwrap();
        let email = processor.process_email(&email_string);
        assert!(email.is_ok());
        // println!("{}", email.unwrap().message_to_string().unwrap());

        let mut client = config::Client::default();
        client.crypto_action = config::CryptoAction::SignEncrypt;
        // Sign and encrypt.
        let processor = Processor::new(&cfg, &client).unwrap();
        let email = processor.process_email(&email_string);
        assert!(email.is_ok());
        // println!("{}", email.unwrap().message_to_string().unwrap());

        cfg.clients = config::Clients::default();
        // No action, the email is built as the original
        let processor =
            Processor::new(&cfg, &cfg.clients.first().unwrap()).unwrap();
        let email = processor.process_email(&email_string);
        assert!(email.is_ok());
        // println!("{}", email.unwrap().message_to_string().unwrap());

        let mut client = config::Client::default();
        client.send_all_or_none = true;
        cfg.clients = config::Clients::new(vec![client]);
        // Encrypt.
        let processor =
            Processor::new(&cfg, &cfg.clients.first().unwrap()).unwrap();
        let email = processor.process_email(&email_string);
        assert!(email.is_err());
        // println!("{}", email.unwrap().message_to_string().unwrap());
    }
}
