//! Manage incoming Email queue.

use failure::Fallible;
use inotify;
use std::{fs, path};

use super::constants::EMAIL_EXT;

// buffer size taken from inotify.rs README
// each event is ~ 16 byte + size_of(filename)
// assuming 16 byte filenames
// this is enough for 4096/32 = 128 events
const INOTIFY_BUFFER_SIZE: usize = 4096;

pub trait Handler {
    fn handle(&self, email: String) -> Fallible<()>;
}

pub struct ReceiveQueue<H>
where
    H: Handler,
{
    handler: H,
    path: path::PathBuf,
}

impl<H> ReceiveQueue<H>
where
    H: Handler,
{
    pub fn new<P>(path: P, handler: H) -> Fallible<Self>
    where
        P: AsRef<path::Path>,
    {
        Ok(Self {
            path: path.as_ref().to_path_buf(),
            handler,
        })
    }

    /// Continously look for mails in queue dir and process them.
    ///
    /// For now the processing happens syncronously.
    /// #43 tracks creating a multi-threated approach.
    pub fn watch(self: &Self) -> Fallible<()> {
        let inotify_result = self.start_watching();
        self.process_existing_files()?;
        // check for inotify errors now that existing files have been processed
        let mut inot = inotify_result?;
        let mut buffer = [0u8; INOTIFY_BUFFER_SIZE];
        loop {
            let events = inot.read_events_blocking(&mut buffer)?;
            self.process_events(events);
        }
    }

    fn start_watching(self: &Self) -> Fallible<inotify::Inotify> {
        let mut inot = inotify::Inotify::init()?;
        let mask =
            inotify::WatchMask::CLOSE_WRITE | inotify::WatchMask::MOVED_TO;
        inot.add_watch(&self.path, mask)?;
        Ok(inot)
    }

    /// Process all the Emails in the queue directory.
    ///
    /// This is intended to be used for a first pass
    /// that processes all files that were created before umschlagend ran.
    ///
    /// It does not detect files that are still open.
    /// So when run while files are still being written
    /// it may process them too early.
    fn process_existing_files(self: &Self) -> Fallible<()> {
        debug!(
            "Processing queue directory {}",
            self.path.as_path().to_str().unwrap()
        );
        let dir = fs::read_dir(&self.path)?;
        for entry in dir {
            if entry.is_err() {
                error!("{:?}", entry.err());
                continue;
            }
            let file = entry.unwrap();
            if !file.file_type()?.is_file() {
                continue;
            }
            if !file.file_name().to_str().unwrap().ends_with(EMAIL_EXT) {
                continue;
            }
            if let Err(err) = self.process_file(&file.path()) {
                error!("Error reading file {:?}: {:?}", &file.path(), err);
            }
        }
        Ok(())
    }

    fn process_events(self: &Self, events: inotify::Events) {
        for event in events {
            debug!("{:?}", event);
            if let Some(name) = event.name {
                debug!("trying to process {:?}", name);
                let path = self.path.join(name);
                let result = self.process_file(&path);
                // log error but proceed with the other events
                if result.is_err() {
                    error!("Error processing {:?}.", &path);
                }
            }
        }
    }

    fn process_file<F>(self: &Self, file: F) -> Fallible<()>
    where
        F: AsRef<path::Path> + std::fmt::Debug,
    {
        trace!("Found email file {:?}", file.as_ref());
        // Ignore one single email fail to be read, but log it.
        let s = fs::read_to_string(&file)?;
        match self.handler.handle(s) {
            Ok(()) => fs::remove_file(&file)?,
            Err(err) => self.handle_err(&file, err)?,
        };
        Ok(())
    }

    fn handle_err<F>(&self, file: F, err: failure::Error) -> Fallible<()>
    where
        F: AsRef<path::Path> + std::fmt::Debug,
    {
        let filename = file.as_ref().file_name().unwrap();
        let path = self.path.join("err");
        if !path.exists() {
            fs::create_dir(&path)?;
        }
        error!("Failed to parse {:?}: {:?}", file, err);
        fs::rename(&file, path.join(filename))?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::{receive_queue, tests_common, Fallible};
    use std::{fs, path};

    struct MyHandler {}

    impl receive_queue::Handler for MyHandler {
        fn handle(&self, _: String) -> Fallible<()> {
            Ok(())
        }
    }

    #[test]
    fn process_existing_files() {
        let home = tests_common::home_with_fixtures();
        let dir = create_queue(&home).unwrap();
        let handler = MyHandler {};
        let queue =
            receive_queue::ReceiveQueue::new(dir.clone(), handler).unwrap();
        let result = queue.process_existing_files();
        assert!(result.is_ok());
        // After processing the queue, it should be empty.
        let mut dir_entries: fs::ReadDir = fs::read_dir(dir).unwrap();
        assert!(dir_entries.next().is_none());
    }

    fn create_queue<P: AsRef<path::Path>>(home: P) -> Fallible<path::PathBuf> {
        let queue = home.as_ref().join(".mail");
        fs::create_dir_all(&queue)?;
        for i in 0..100 {
            fs::copy(
                tests_common::fixture("email_plain.eml"),
                queue.join(format!("{}.eml", i)),
            )?;
        }
        Ok(queue)
    }
}
