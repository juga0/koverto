# Installation

While there is no system package, follow these instructions:

## Install the system dependencies

In Debian buster or Ubuntu

```
sudo apt install -yqq capnproto clang make pkg-config nettle-dev libssl-dev capnproto libsqlite3-dev rustc cargo libssl1.1
```

## Download the code

```
git clone https://gitlab.com/juga0/umschlagend
```

## Compile the code

```
cd umschlagend
cargo build
```

## Run via cli

Run the Mail Transfer Agent (MTA) ie. the process but will be
listening for incoming Emails and store them in $HOME/.mail.

```
cargo run -- mta
```

Run the process that will encrypt and/or sign the Emails found in $HOME/.mail
and send them to the recipienets.

```
cargo run
```

See [umschlagend](umschlagend.1.md) (or `man doc/src/umschlagend.1.md`).

## Install

To install systemd services run by user `umschlagend` run:

```
sudo WITH_SYSTEMD=1 make install
```

## Configuration

There is no need for configuration file.

To set custom configuration see [umschlagend.toml](umschlagend.toml.5.md) (or
`man doc/src/umschlagend.toml.5.md`).

To run the receive-only Mail Transfer Agent (MTA), a certificate file is
needed.

Use [Let's Encrypt](https://letsencrypt.org) to create a valid
certificate file.

For local tests, you can create a self-signed certificate with openssl, eg.:

```
openssl req -nodes -x509 -newkey rsa:2048 -keyout key.pem -subj '/CN=localhost' -out cert.pem
openssl pkcs12 -export -out umschlagend.pfx -inkey key.pem -in cert.pem
```

ADVICE: Any user can send Email to the receive-only MTA, therefore it is
recommended to run a firewal that will at least restrict the network socket
listening for Email to the IP of the servers sending Email to the server where
`umschlagend` run.
